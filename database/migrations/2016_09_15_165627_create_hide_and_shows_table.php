<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHideAndShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hide_and_shows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_tag')->unsigned();
            $table->foreign('user_tag')->references('id')->on('users')->onDelete('cascade');
            $table->integer('tweet_tag')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hide_and_shows');
    }
}
