<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowerAndFollowingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follower_and_followings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Following_user_id')->unsigned();
            $table->foreign('Following_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('Follower_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('follower_and_followings');
    }
}
