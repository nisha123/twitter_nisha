<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Following_notification_id')->unsigned();
            $table->foreign('Following_notification_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('Follower_notification_id')->unsigned();
            $table->boolean('read')->default(0);
            $table->string('notification_type');
            $table->string('notification');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
