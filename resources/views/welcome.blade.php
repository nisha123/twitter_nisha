@inject('name','App\ACME\UserHelper')
@extends('layouts.app')
@section('content')
    <div class="container" >
        <div class="row">

            <div class="col-md-3">
                <div class="row">
                    <div class="col-sm-4 col-md-12">

                        <div class="thumbnail panel " style="background-color:rgb(109,207,246)">
                            @if($userImagePath == null)

                                <img src="{{url('dummy.jpg')}}">
                            @else

                                <div class="gallery_image col-md-offset-4">
                                    <img src="{{url($userImagePath)}}" width="90" height="80">
                                </div>
                            @endif
                            {{--</form>--}}

                            <div class="caption">
                                <h3>{{Auth::user()->name}}</h3>


                                <p>
                                    <h>{{Auth::user()->email}}</h>
                                </p>
                                <script>
                                    document.getElementById("uploadBtn").onchange = function () {
                                        document.getElementById("uploadFile").value = this.value;
                                    };
                                </script>


                                {{Form::open(array('url'=> 'uploadImage/'.Auth::user()->id , 'files' => 'true'))}}


                                <div class="form-group ">
                                    {!! Form::file('image')!!}
{{--                                    {!! Form::submit('upload') !!}--}}
                                    {{--<input type="button" value="Browse" class="btn btn-primary"  name="image">--}}
                                    {!! Form::submit('upload',array('class'=>'btn btn-primary') )!!}
                                </div>

                                {{Form::close()}}

                                {{Form::open(array('method'=>'get','url'=>'edit/profile'))}}

                                {{Form::submit('Edit Profile',array('class' =>'btn btn-primary'))}}

                                {{Form::close()}}




                                {{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
                <div class=" panel panel-primary col-md-12" >
                    {{--<p>--}}
                  <div class="panel-heading row"  >
                    <a class="col-md-4" href="{{ url('/tweetPage') }}" style="color: white">Tweets</a>
                    <a class="col-md-4" href="{{ url('/follower') }}" style="color: white">Followers</a>
                    <a class="col-md-4" href="{{ url('/following') }}" style="color: white">Following</a>
                    </div>
                    {{--</p>--}}


                <div class=" panel-body ">
                    <div class="col-md-4">{{$noOfTweets }}</div>
                    <div class="col-md-4">{{$noOfFollowers }}</div>
                    <div class="col-md-4">{{$noOfFollowings }}</div>
                </div>
                </div>
            </div>


            @include('partials.userTweet')
            {{--<form action="friendlist" method="post">--}}
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel " style="background-color: rgb(51,122,183)"><h3>Registered User</h3></div>
                    <div>
                        @foreach($showFriendList as $friends)
                            @if($fetchMobileVerifyToken == 1)

                                <div class="" role="alert" >
                                    <a href="{{url('username/'.$friends->name)}}">{{$friends->name}}</a>
                                </div>
                            @else

                                {{--                            <div> <a>{{$friends->name}}</a>--}}

                                <div><a data-toggle="modal" data-target=".bs-example-modal-lg">{{$friends->name}}</a>
                                </div>


                            @endif
                        @endforeach
                    </div>

                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
                         aria-labelledby="myLargeModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content modal-header">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Mobile number verification</h4>
                                </div>
                                <h4 class="modal-title"> Please Verify Your Mobile Number First To Follow Registered
                                    User</h4>
                            </div>
                        </div>
                    </div>

                    {{--</form>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFilesize: 3,
            acceptedFiles: '.JPG,.jpg , .jpeg , .png, .bmp'
        };
    </script>
@stop

