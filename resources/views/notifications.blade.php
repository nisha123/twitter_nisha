@inject('name','App\ACME\UserHelper')

@extends('layouts.app')
@section('content')
    <div class="col-md-6 col-md-offset-3 panel ">
<div class="alert col-md-offset-3"  style="color:rgb(59,120,231)"><h2><b>All Notifications</b></h2></div>
    @foreach($name->fetchFollowingNotificationId() as $id)
        <div class=" alert widget" style="background-color:rgb(255,255,102)">
            <a class=""><b>{{$name->getFollowerUserNameViaUserId($id->Following_notification_id)}}</b> starting following you</a>
        </div>
    @endforeach
    </div>

@endsection