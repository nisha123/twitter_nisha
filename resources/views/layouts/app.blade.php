@inject('name','App\ACME\UserHelper')
        <!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    {{--<link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">--}}

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    {{--<link rel="stylesheet" href="{{url('css/font-awesome.css')}}">--}}
    {{--<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">--}}

    <link rel="stylesheet" href="{{url('css/dropzone.css')}}">
    <link href="{{url('css/typeaheadjs.css')}}" rel="stylesheet" type="text/css"/>
{{--    <link rel="stylesheet" href="{{url('css/datepicker.css')}}">--}}
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css')}}">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        .top-buffer {
            margin-top: 10px;
        }

        .bcground {
            background: url("tweet.png") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

    <script type="text/javascript" src="{{url('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
{{--    <script type="text/javascript" src="{{url('js/datepicker.js')}}"></script>--}}
    <script type="text/javascript" src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript" src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js')}}"></script>


    {{--<script type="text/javascript" src="{{url('js/typeahead.js')}}"></script>--}}
</head>
<body id="app-layout" class="bcground">

<nav class="navbar navbar-default navbar-fixed">
    <div class="container ">
        <div class="navbar-header">

        <div class="col-md-3">
            <ul class="nav navbar-nav ">
                <li><a class="navbar-brand" style="color: rgb(51,122,183) " href="{{ url('/welcome') }}">Home</a></li>
            </ul>
        </div>


          {{--<div class="glyphicon glyphicon-bell"></div>--}}

        <div class="col-md-3">
                    <ul class="nav navbar-nav ">
                        <li><a class="navbar-brand" style="color: rgb(51,122,183) " href="{{ url('/welcome') }}">Message</a></li>
                        {{--<a class=" navbar-brand"><h4>message </h4></a>--}}
                    </ul>
        </div>

{{--                            {{dd($name->countNotifications())}}--}}
{{--                            {{dd($name->fetchReadTokenFromNotifications())}}--}}
            <div class="dropdown col-md-6 top-buffer">
                @if(Auth::check())
            @if($name->countNotifications() !== 0 && $name->fetchReadTokenFromNotifications() == 0 )
                 <a class=" dropdown-toggle  " data-toggle="dropdown"><h4>Notifications<span class="badge" style="background-color: rgb(240,173,78)">{{$name->countNotifications()}}</span></h4></a>
                    @else
                 <a class=" dropdown-toggle  " data-toggle="dropdown"><h4>Notifications</h4></a>
            @endif
                    <ul class="dropdown-menu">
                            @foreach($name->fetchFollowingNotificationId()->take(2) as $id)
                                 <li><a href="#"><b>{{$name->getFollowerUserNameViaUserId($id->Following_notification_id)}}</b> starting following you</a></li>
                            @endforeach
                                <li class="divider"></li>
                                <li><a href="{{url('notifications')}}">See all</a></li>
                    </ul>
                    @endif
        </div>
    </div>
@if(Auth::check() == true)
        <div class="collapse navbar-collapse " id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
{{--            {{dd($searchedName)}}--}}
<div class="row col-md-8">

            <form action="{{url('find/users')}}" method="post"   role="search">
     {{csrf_field()}}
                <div class="col-md-8 col-md-offset-2 row top-buffer">
                            <input type="text" id="typeahead"  class="form-control typeahead "
                                   placeholder="Search for..."  name="typeahead" autocomplete="off">
                    {{--</div>--}}
                </div>
                <div class="col-md-2 top-buffer">
                    <button class="btn btn-default" type="submit">Go!</button>
                    {{--<a class="btn btn-primary"  href="{{url('username/typehead')}}">GO</a>--}}
                </div>
            </form>
</div>

{{--{{dd($fetchId)}}--}}
            <script type="text/javascript" src="{{url('js/typeahead.min.js')}}"></script>

            <script>


                $('input.typeahead').typeahead({
                        name: 'typeahead',
                        remote: document.location.origin + '/project_twitter/public/find/users/%QUERY',
                        limit : 10

                    });
            </script>
@endif
            {{--<img src="{{$fetchImagePathOFUser}}" class="img-circle " alt="Cinque Terre" width="60" height="50">--}}

            <ul class="nav navbar-nav navbar-right ">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    @yield('content')

</div>


@yield('footer')

</body>
</html>
