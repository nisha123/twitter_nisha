@extends('layouts.app')
@section('content')
    <div class=" panel-default col-md-8  col-md-offset-2"  >
        <div class="panel-heading  " style="background-color :rgb(103,194,241)">About</div>
        <div class="panel-body " style="background-color: rgb(248,248,248)" >
            <div class="col-md-12">

                <div class="col-md-offset-1">
<div class="col-md-2">
    @foreach($UserName->photos->chunk(5) as $set)

            <div class="row ">
                @foreach($set as $photo)
                    <div class="col-md-3 gallery_image ">
                        <img src ="{{url( $photo->path )}}" width="80" height="80">
                    </div>
                @endforeach
            </div>
    @endforeach
</div>
{{--                    {{dd($searchUsername)}}--}}

                    <div class="col-md-10">
        <div class="panel">
            {{--        {{ $UserName->id}}--}}
            <h4>Name : {!!$UserName->name!!}</h4>
        </div>


                <div class="panel"> <h4>Address :  {!!$UserName->address!!}</h4></div>
                <div class="panel "><h4>Mobile No :   {!!$UserName->mobile_no!!}</h4></div>
                <div class="panel"> <h4> Date of birth :  {!!$UserName->dob!!}</h4></div>
                <div class="panel"> <h4>Gender :  {!!$UserName->gender!!}</h4></div>
                <div class="panel "><h4>Status :  {!!$UserName->status!!}</h4></div>


        @if( $FollowingsIds->pluck('Follower_user_id')->contains($UserName->id)  )
            {{--        @if( $UserName->id == $id->Follower_user_id )--}}

            <input type="button" class="btn btn-primary" value="followed">

        @else

            @if( Auth::user()->name != $UserName->name )

                {{--        <form action="{{url('follower')}}" method="post">--}}
                <form action="{{url('username/'.$UserName->name)}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="Follower_notification_id" value="{{$UserName->id}}" >
                    <input type="hidden" name="Follower_user_id" value="{{$UserName->id}}" >
                    <input type="hidden" name="Following_notification_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="Following_user_id" value="{{Auth::user()->id}}">
                    <button type="submit" name="follower_user_id" class="btn btn-primary">Follow</button>
                </form>
            @endif

        @endif
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <div class=" navbar-fixed-bottom panel-footer"><h>user profile</h></div>

@endsection