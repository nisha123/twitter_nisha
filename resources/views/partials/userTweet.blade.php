@inject('obj','App\Photo')
@inject('name','App\User')
@inject('tweetUsrId','App\HideAndShow')
@inject('name','App\ACME\UserHelper')

<div class="col-md-6">
    <form action="{{url('tweet')}}" method="post">
        {{csrf_field()}}

        <div class="panel panel-primary">
            <div class="container">
                <div class="row col-md-7 ">
                    @if($fetchImagePathOFUser == null)
                        <div class="col-md-1 "><img src="{{url('dummy.jpg')}}" class="img-rounded col-sm-offset-left-4"
                                                    width="30" height="50"></div>
                    @else
                        <div class="col-md-1 "><img src="{{$fetchImagePathOFUser}}"
                                                    class="img-rounded col-sm-offset-left-4" alt="Cinque Terre"
                                                    width="30" height="50"></div>
                    @endif
                    {{--<input type="hidden" name="user_id" value="{{Auth::user()->id}}">--}}
                    <div class="col-md-7 col-md-offset-left-1 row top-buffer"><input type="text" name="tweet"
                                                                                     placeholder="enter your tweet"
                                                                                     class="form-control" required>
                    </div>
                    <div class="col-md-3 top-buffer">
                        <button type="submit" class="btn btn-primary">Tweet</button>
                    </div>

                </div>
            </div>
        </div>

    </form>
    <div>

        @if($tweetText->isEmpty())
            <h1>no tweets to show</h1>

        @else
            @foreach($tweetText as $show)
                {{--                {{dd($show->all())}}--}}
                <div class="alert alert-info" role="alert" id="hide">

                    @if(in_array(Auth::user()->id,$tweetUsrId->fetchTweetTag($show->user_id,$show->id )) == true )
                        <form action="{{url('unhide/button')}}" method="post">
                            {{csrf_field()}}
                            {{--<input type="hidden" name="hideId" value="{{$tweetUsrId->fetchTweetTag($show->user_id,$show->id)}}">--}}
                            <input type="hidden" name="hideId" value="{{$show->user_id}}">
                            <input type="hidden" name="Id" value="{{$show->id}}">

                            <button type="submit" onclick="myFunction()" class="close">unhide</button>
                        </form>
                    @else

                        <form action="{{url('hide/button')}}" method="post">
                            {{csrf_field()}}

                            <input type="hidden" name="hideId" value="{{$show->user_id}}">
                            <input type="hidden" name="LoginId" value="{{Auth::user()->id}}">

                            <input type="hidden" name="Id" value="{{$show->id}}">
                            <button type="submit" class="close">Hide</button>

                        </form>


                        <h4 style="color: black">{{$name->getFollowerUserNameViaUserId($show->user_id)}}</h4>
                        @if($obj->getProfilePicViaUserId($show->user_id) == null)
                            <img src="{{url('dummy.jpg')}}" width="40" height="30">
                        @else
                            <img src="{{$obj->getProfilePicViaUserId($show->user_id)}}">
                        @endif
                        <strong>{{$show->tweet}}</strong>
                        <div class="col-md-12">
                            @foreach( $name->getCommentFromTweet($show->id) as $key )
                                <div class="panel panel-body col-md-12">
                                    <div class=" tab col-md-10">
                                        <strong><em>{{$name->getFollowerUserNameViaUserId($key->user_id )}}</em></strong>
                                        &nbsp;&nbsp;&nbsp;&nbsp;{{$key->comment}}</div>
                                    <a class="col-md-2" href="{{url('delete/comment'.$key->comment_id)}}">delete</a>
                                </div>

                            @endforeach
                        </div>

                        <div class="col-md-offset-9 top-buffer">{{$show->created_at->diffForHumans()}}</div>
                        <button class="btn btn-primary" data-toggle="modal" data-exp="{{$show->id}}"
                                data-whatever="{{$name->getFollowerUserNameViaUserId($show->user_id)}}"
                                data-target="#exampleModal"><i class="fa fa-reply" aria-hidden="true"></i></button>

                        {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>--}}
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                                    </div>
                                    <form action="{{url('user/comment')}}" method="post">
                                        {{csrf_field()}}
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="control-label">Recipient:</label>
                                                <input type="text" name="userNmae" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="control-label">Comment:</label>
                                                <textarea class="form-control" name="text" id="message-text"></textarea>
                                            </div>
                                            <span><input type="hidden" name="tweet_Id" class="form-control"></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            {{--<input type="hidden"  name="tweet_Id" id="abc-name"--}}
                                            {{--data-target="#exampleModal" value="{{$show->id}}">--}}
                                            <button type="submit" class="btn btn-primary">Comment</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <script>

                </script>
            @endforeach
        @endif
    </div>
</div>
<script>
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var abc = button.data('exp') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Reply to ' + recipient)
        modal.find('.modal-body input').val(recipient)
        modal.find('.modal-body span input ').val(abc)
    })
</script>