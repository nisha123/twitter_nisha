@extends('layouts.app')
@section('content')
<html>
<body>

    @if(Session::has('message'))
        <div class="alert alert-danger">{{Session::get('message')}}</div>
    @endif

    {{--You will be logged out in <span id="hms"></span>--}}

<form action="{{url('send/otp')}}" method="post">
    {{csrf_field()}}

    <div class="panel panel-heading col-md-6 " style="background-color:rgb(248,248,248) " >

            Enter Your OTP number {{Form::text('otp')}}
            {{Form::submit('submit',array('class'=>'btn btn-primary'))}}
        @if ($errors->has('otp'))<p style="color:red;">{!!$errors->first('otp')!!}</p>@endif
        {{--<div id="hms" class="col-md-6"></div>--}}
    </div>
</form>

@if(Session::has('otp'))
    {{--<div></div>--}}
{{--    <form action="{{url('refresh')}}">--}}
    <div class="col-md-5">OTP expires in <span id="counter">60</span> sec!</div>
{{--        {{Form::submit('Refresh',array('class' => 'btn btn-primary'))}}--}}
    {{--</form>--}}
    {{--<div id="counter">5</div>--}}
@else
    <form action="{{url('create/otp')}}">
        <div  class="panel panel-heading col-md-6" style="background-color:rgb(248,248,248) ">
            {{Form::submit('Send OTP',array('class' => 'btn btn-primary'))}}


        </div>
    </form>
@endif
<script src="{{url('js/countdown.js')}}"></script>
<script type="text/javascript">


        function countdown() {
            var i = document.getElementById('counter');
            i.innerHTML = parseInt(i.innerHTML)-1;
            if (parseInt(i.innerHTML)==0) {
                clearInterval(timerId);
                alert('resend OTP')
                window.location = "create/otp";
            }
        }
var timerId = setInterval(function(){ countdown(); },1000);

//



</script>
{{--</div>--}}

</body>
</html>
@endsection