@inject('name','App\User')
@inject('photos','App\Photo')
@inject('name','App\ACME\UserHelper')

@extends('layouts.app')
@section('content')
    <html>
    <head>

    </head>
    <body>
    <div class="container">
        <div class="row col-md-12 alert alert-info" role="alert">
            <div class="col-md-2"><h3 style="color: blue">Following</h3></div>
            <div class="col-md-4"><h3>{{$noOfFollowings }}</h3></div>
        </div>


        @foreach($FollowingsIds as $id)
            <div class="row col-md-12 alert alert-success" >

                <div class="col-md-2"><img src="{{$photos->getProfilePicViaUserId($id->Follower_user_id)}}"></div>
                <div class="col-md-2"><h1>{{$name->getFollowerUserNameViaUserId($id->Follower_user_id)}}</h1></div>
{{--                <div class="col-md-6">{{$id->Follower_user_id}}</div>--}}
            </div>

        @endforeach
    </div>
    </body>
    </html>
@endsection