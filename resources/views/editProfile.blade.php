@extends('layouts.app')
@section('content')
<div class="container">
    @if($errors->all())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif

        @if(Session::has('messageSuccess'))
            <div class="alert alert-success">{{Session::get('messageSuccess')}}</div>

        @endif

        <div class="panel-heading" style="background-color:#B0BEC5 ">Edit Profile</div>
        <div class=" panel-body" style="background-color: rgb(248,248,248)">

        <div class="col-md-offset-1 col-md-10 well " xmlns="http://www.w3.org/1999/html">

        <div class="row ">

    {{Form::model($user ,array('method'=>'post','url'=>'save/edit/page/'.$user->id,'class'=>'form'))}}

    <div class="form-group  col-md-12">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-12">
        {!! Form::label('address', 'Address:') !!}
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-12">
        {!! Form::label('mobile_no', 'Mobile No:') !!}
        {!! Form::text('mobile_no', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-12 ">
        <b>Date Of Birth:</b>
        <div class="input-append date"  data-date="12-09-2016" data-date-format="dd-mm-yyyy">
            {!! Form::text('dob', null, ['class' => 'form-control' ,'date','input-append','add-on','id' => 'datepicker']) !!}

            <span class="add-on"><i class="icon-th"></i></span>
        </div>
{{--      {!!   Form::label('id', 'Description', array('class' => 'datepicker'))!!}--}}
{{--        {!! Form::label('dob', ['id' => 'datepicker'] !!}--}}
{{--       {!! Form::text('date', 'null', array('id' => 'datepicker')!!}--}}
    </div>

    <div class="form-group col-md-12">
        {!! Form::label('status', 'Status:') !!}
        {!! Form::select('status',['Single' => 'Single','In Relationship' => 'In Relationship','Married' => 'Married','complicated'=>'complicated']) !!}
    </div>

    <div class="form-group col-md-12 ">
        {!! Form::label('title', 'Gender:') !!}
        {!! Form::radio('gender','Male') !!}
        {!! Form::label('title', 'Male') !!}
        {!! Form::radio('gender','Female',true) !!}
        {!! Form::label('title', 'Female') !!}

    </div>

        </div>

        <div class="row col-md-12">
            <div class="col-md-2">
                {!! Form::submit('Save Changes',array('class'=>'btn btn-primary')) !!}
             </div>

            <div class="col-md-6">
                {!! Form::button('Cancel',array('class'=>'btn btn-primary')) !!}
            </div>
            <div><a class="btn btn-primary" href="{{ url('sms') }}">Mobile verification</a></div>

        </div>
    </div>
        </div>

        {{Form::close()}}



</div>
{{--<script>--}}
    {{--//                $('.datepicker').datepicker()--}}
    {{--$('#datepicker').datepicker();--}}
{{--</script>--}}

<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker({
//            changeMonth: true,
//            changeYear: true
        });
    });
</script>
@endsection
