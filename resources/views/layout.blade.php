<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta charset="utf-8">

<link rel="stylesheet"  href="{{url('css/bootstrap.min.css')}}">
<link rel="stylesheet"  href="{{url('css/dropzone.css')}}">
{{--    <link rel="stylesheet" href="{{url('font-awesome.css')}}">--}}
    <link rel="stylesheet" href="{{url('font-awesome.min.css')}}">
{{--    <link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}">--}}
    <link rel="stylesheet" href="{{url('css/datepicker.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css')}}">

    <style>

    .top-buffer { margin-top:-40%; }

    .top-upbuffer { margin-top:4%; }

    .bcground {
        background: url("image.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

</style>

</head>
<body class="bcground">
<div class="container bcground   ">


    <div>
        <img src="/header.png" class="img-responsive bcground" alt="Responsive image" >

    </div>


    <form class="navbar-form navbar-left ">
        {{csrf_field()}}

         <div class="row col-md-offset-5 col-md-8 top-buffer" >
             @if (Auth::guest())

                <div class="form-group " >
                    <h1>What's Happening</h1>



                     <a href="{{ url('/login') }}" class="btn btn-default">Login</a>
                     <a href="{{ url('/register') }}" class="btn btn-default">Register</a>
                </div>


             @else
                 <div class="col-md-4">
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                             {{ Auth::user()->name }} <span class="caret"></span>
                         </a>

                         <ul class="dropdown-menu" role="menu">
                             <a href="{{ url('/logout') }}" class="fa fa-btn fa-sign-out">Logout</a>
                             {{--<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>--}}
                         </ul>
                     </li>

                 </div>
             @endif

         </div>


    </form>
    <div class="col-md-offset-10">
        {{--<li>--}}
       <a  style="color: white" href="{{url('contact')}}"><h1><strong>Contact us</strong></h1></a>
        {{--</li>--}}
    </div>
</div>


@yield('contents')
<script type="text/javascript" src="{{url('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/dropzone.js')}}"></script>
{{--<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.min.js')}}"></script>--}}
<script type="text/javascript" src="{{url('js/datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js')}}"></script>


</body>

</html>