<?php

namespace App\Classes;
//use Intervention\Image\Facades\Image;
use App\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Thumbnail;
use App\Photo;
class AddPhotoToUserProfile
{
    protected $user;
    protected $file;

    public function __construct(User $user, UploadedFile $file, Thumbnail $thumbnail = null)
    {
        $this->user = $user ;
        $this->file = $file;
        $this->thumbnail = $thumbnail ?: new Thumbnail;
//            dd($this->thumbnail);
    }


    public function save()
    {

        $photo = $this->user->addPhoto($this->makePhoto());
//          dd($this->user);
        $this->file->move($photo->baseDir(), $photo->name);

        $this->thumbnail->make($photo->path, $photo->thumbnail_path);

    }


    public function makePhoto()
        {
            return new Photo(['name' => $this->makeFileName()]);
        }

    public function makeFileName()
        {
            $name = sha1(
                time(). $this->file->getClientOriginalName()
            );

            $extension = $this->file->getClientOriginalExtension();

            return "{$name}.{$extension}";
        }
}