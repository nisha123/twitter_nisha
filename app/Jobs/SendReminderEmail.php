<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReminderEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
//    public function handle(Mailer $mailer)
//
//    {
//
//        $mailer->send('emails.reminder',['user'=> $this->user],function($message)
//        {
//            $message->to('nishagahlot1212@gmail.com','nisha')->subject('welcome');
//        });
//
////        $this->user->reminders()->create(......);
//    }
}
