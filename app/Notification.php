<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Notification extends Model
{
   protected $fillable =['Following_notification_id',
       'Follower_notification_id','read'];

//    public static function fetchFollowingNotificationId()
//    {
//        return Notification::where('Follower_notification_id', Auth::user()->id)->get()->sortByDesc('id')->take(2);
//    }
//
    public static function showCommentTime()
    {
       return Notification::where('Follower_notification_id', Auth::user()->id)->count();
    }

    public function UpdateReadNotification()
    {
       return Notification::where('Follower_notification_id',Auth::user()->id)->update(['read' => 1]);
    }

    public function ReUpdateReadNotification()
    {
        return Notification::where('Follower_notification_id',Auth::user()->id)->update(['read' => 1]);
    }
}


