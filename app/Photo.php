<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Photo extends Model
{
    protected $table = 'user_photos';

    protected $fillable = ['path', 'name', 'user_id', 'thumbnail_path' ];

    protected $baseDir = 'userpics/photos';


    public  function User()
    {

        return $this->belongsTo('App\User');
//        return $this->hasMany('App\User');
    }




    public function setNameAttribute($name)
    {

        $this->attributes['name'] = $name;
//        dd($this);// return null
        $this->path = $this->baseDir() .'/'. $name;

        $this->thumbnail_path = $this->baseDir() .'/tn-'. $name;
    }

    public function getProfilePicViaUserId($userId)
    {

        return Photo::where('user_id', $userId)

            ->value('thumbnail_path');
    }



    public function baseDir()
    {
        return 'userpics/photos';
    }

    public static function getImagePath()
    {
//        dd(Auth::user()->id);
//        dd(Photo::where('user_id',Auth::user()->id)
//            ->value('path'));
        return Photo::where('user_id',Auth::user()->id)
                      ->value('path');

    }


}
