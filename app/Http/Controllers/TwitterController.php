<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App;
use App\User;
use App\twitter;
use App\Http\Requests;
use App\FollowerAndFollowing;
use App\Photo;
use Illuminate\Support\Facades\Input;
use App\Notification;
use App\UserPostComment;
class TwitterController extends Controller
{

    public  function show($name)
    {
//        $searchname = Input::get('typeahead');
//        $searchUsername = $name;
        $UserName = App\User::locatedAt($name);
        $FollowingsIds = FollowerAndFollowing::getRecords('Following_user_id', Auth::user()->id);

        return view('userProfile',compact('UserName','FollowingsIds'));
    }

    public  function showTweet()
    {
        return redirect('welcome');
    }

    public function gettweetPage()
    {
        $tweetText = twitter::fetchLoggedInUserTweets(Auth::user()->id);

           return view('tweets',compact('tweetText'));
    }

    public function CreateTweet(Request $request)
    {
        $userId = User::where('id', '=', Auth::user()->id)
                                    ->get();
                foreach($userId as $idvalue)
                        twitter::create([
                        'tweet' =>$request['tweet'],
                        'user_id' => $idvalue->id,

                                  ]);

                return redirect('welcome');
    }

    public  function closeButton(Request $request)
    {
        $findId = $request->hideId;
//        dd($request->all());
//        $HideAndShow = new App\HideAndShow();
//        $HideAndShow->user_tag = Auth::user()->id;
//        $HideAndShow->tweet_tag = $request->Id;
//        $HideAndShow->save();
       $hideValue = twitter::where('user_id',$findId)->where('id', $request->Id)->value('hide');
//        dd($hideValue);
        if($hideValue == null)
        {
            twitter::where('user_id',$findId)->where('id', $request->Id)->update(['hide' =>','.$request->LoginId]);
        return back();
        }
        else
        {
            $getHideValue = twitter::where('user_id',$findId)->where('id', $request->Id)->value('hide');
//            dd($getHideValue);
            $array = array_prepend([$getHideValue],$request->LoginId );
//            dd($array);
            $stringArray = implode($array);
            $var = ','.$stringArray;
            twitter::where('user_id',$findId)->where('id', $request->Id)->update(['hide' =>  $var]);
            return back();
        }

    }

    public function fetchName(Request $request)
    {
//        dd($request->typeahead);
        if($request->typeahead == "") {
            return redirect('welcome');
        }else
        {
            return redirect('username/' . $request->typeahead);
        }
    }

    public function unHide(Request $request)
    {
        $findId = $request->hideId;
        $hide = twitter::where('user_id',$findId)->where('id', $request->Id)->update(['hide' => 1]);
        return back();
    }

    public function CreateComments(Request $request)
    {
        $comment = new App\UserPostComment();
        $comment->tweet_id = $request->tweet_Id;
        $comment->user_id = Auth::user()->id;
        $comment->comment = $request->text;
        $comment->save();

//        $abc = UserPostComment::where('tweet_id',$request->tweet_Id)->value('comment');
//        dd($abc);
        return back();
    }

    public function deleteComments($commentId)
    {

          UserPostComment::where('comment_id',$commentId)
             ->delete();

        return back();

    }

}
