<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactMeRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Job;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;

class ContactController extends Controller implements ShouldQueue
{
    public function showForm()
    {
        return view('blog.contact');
    }

    public function sendContactInfo(ContactMeRequest $request )
    {
        $file = $request->file('fileToUpload');

        $data = $request->only('name', 'email', 'phone', 'fileToUpload');

        $destinationPath = 'uploadFiles\files';

        $dc = $file->move($destinationPath,$file->getClientOriginalName());

        $inputfile = $dc->getpathname();

        $data['messageLines'] = explode("\n", $request->get('message'));

        Mail::send('auth.emails.contactEmail', $data, function ($message) use ($data , $inputfile) {
            $message->subject('Blog Contact Form: ' . $data['name']);
            $message  ->to('nishagahlot1212@gmail.com', 'nisha');
            $message ->replyTo($data['email']);

            $message ->attach($inputfile);
        });

        \Session::flash('message', 'please enter correct otp number');
        return redirect()->back();
    }


}