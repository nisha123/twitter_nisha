<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\FollowerAndFollowing;
use App\Notification;
use App\User;

class FollowingAndFollowerController extends Controller
{
    public function setUserFollowerRequest(Request $request)
    {
//                   dd($request);
        FollowerAndFollowing::create($request->all());
        Notification::create($request->all());
            $object =new Notification;
        $object->ReUpdateReadNotification();
//        $userId = User::where('id', '=', Auth::user()->id)
//            ->value('id');

//        $follower = new FollowerAndFollowing();

//        $follower->Follower_user_id = $UserName;
//        $follower->Following_user_id = $userId;
//        $follower->save();
        \Session::flash('flash_success','Followed successfully');
        return redirect('welcome');
    }

    public function getFollowerPage()
    {
        $noOfFollowers = FollowerAndFollowing::getRecords('Follower_user_id', Auth::user()->id)->count();
        $FollowersIds = FollowerAndFollowing::getRecords('Follower_user_id', Auth::user()->id);


        return view('follower',compact('noOfFollowers','FollowersIds'));
    }

    public function getFollowingPage()
    {
        $noOfFollowings = FollowerAndFollowing::getRecords('Following_user_id', Auth::user()->id)->count();
        $FollowingsIds = FollowerAndFollowing::getRecords('Following_user_id', Auth::user()->id);

        return view('following',compact('noOfFollowings','FollowingsIds'));
    }
}
