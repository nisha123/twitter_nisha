<?php

namespace App\Http\Controllers;

use App\twitter;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public static function findSearch($query)
    {
        $userSearch = User::where('name', 'LIKE', '%' . $query . '%')->get()->pluck('name')->all();

        echo json_encode($userSearch);

    }
}
