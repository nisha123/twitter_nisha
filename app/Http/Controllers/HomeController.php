<?php

namespace App\Http\Controllers;
use App\Classes\AddPhotoToUserProfile;
use App\FollowerAndFollowing;
use App\Jobs\SendReminderEmail;
use App\Notification;
use App\Photo;
use App\User;
use App\HideAndShow;
use Illuminate\Http\Request;
use Auth;
use App\twitter;
use App\Http\Requests;
use Mail;
use Illuminate\Support\Facades\Input;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

           $userImagePath = Photo::where('user_id',Auth::user()->id)->value('path');

            $tweetText = twitter::fetchAllTweetsFromUserId(Auth::user()->id);

            $noOfTweets = twitter::calculateNoOfTweets(Auth::user()->id);

            $noOfFollowers = FollowerAndFollowing::getRecords('Follower_user_id', Auth::user()->id)->count();

            $noOfFollowings = FollowerAndFollowing::getRecords('Following_user_id', Auth::user()->id)->count();

            $showFriendList = User::fetchNameFromUserTable();

            $fetchImagePathOFUser = Photo::getImagePath();

            $fetchMobileVerifyToken = User::fetchMobileVerifyTokenFromTable(Auth::user()->id);

            return view('welcome', compact('searchQuery', 'tweetText','noOfTweets','noOfFollowers', 'noOfFollowings',
              'allIdOfTweet',  'TweetTagId','showFriendList','userImagePath' ,'fetchImagePathOFUser','fetchMobileVerifyToken'));

        }


}
