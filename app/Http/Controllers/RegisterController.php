<?php

namespace App\Http\Controllers;

use App\User;
//use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;
use InvalidConfirmationCodeException;
use Auth;
use App\Http\Requests\OtpRequest;
use SimpleSoftwareIO\SMS\Facades\SMS;
use App\Notification;

class RegisterController extends Controller
{
  public function confirm($verify_token)
  {
      if(!$verify_token)
      {
          throw new InvalidConfirmationCodeException;
      }

      $user = User::where('verify_token',$verify_token)->first();

      if(!$user )
      {
          throw new InvalidConfirmationCodeException ;
      }

      $user->verify = 1;
      $user->verify_token = null;
      $user->save();

         \Session::flash('flash_message','you have successfully verify your account');

      return redirect('welcome');


  }

    public function viewEdit(Request $request)
    {

        $user = User::find(Auth::user()->id);

        return view('editProfile',compact('user'));
    }

    public function updateRegister(Requests\EditPageRequest $request , User $user)
    {
        if($request->mobile_no == $user->mobile_no)
        {
//            dd($user);
            $user->update($request->all());


            return back();

        }else{

            $user = User::where('id', Auth::user()->id)->first();
            $user->mobile_verify = 0;
            $user->save();
            $user->update($request->all());

            return back();
        }

    }

    public function showOptPage()
    {
        return view('auth.sms');
    }

    public function createOtp()
    {
    if(Session::get('LAST_ACTIVITY')&&(time()- Session::get('LAST_ACTIVITY'))>60)
    {
        Session::forget('otp');
        Session::forget('LAST_ACTIVITY');
        return back();
    }else{

        $token = mt_rand(100000, 999999);
        Session::put('otp', $token);
        Session::put('LAST_ACTIVITY', time());
        SMS::send('your otp code is :' . $token, null, function ($sms) {
            $sms->to(Auth::user()->mobile_no);
        });
    }

        return back();
    }



    public function otpVerification(Request $request)
    {
        $this->validate($request,['otp'=>'required']);

         $enteredOtp = $request->otp;
         $sessionOtp = Session::get('otp'); //Retrieving An Item And Forgetting It

        if($enteredOtp == $sessionOtp)
        {
               $token = User::find(Auth::user()->id);
               $token->mobile_verify = 1;
               $token->save();
            \Session::forget('otp');
            \Session::forget('LAST_ACTIVITY');
//            \Session::forget('token');

            \Session::flash('messageSuccess','your mobile number is verified , please fill your details');


        }else
        {
            \Session::flash('message','please enter correct otp number');
             return back();
        }
//
//        SMS::send('Your SMS Message', null, function($sms) {
//            $sms->to('+15555555555');});
        SMS::send('congratulations!! your otp is verified',null,function($sms){$sms->to(Auth::user()->mobile_no);});


        $user = User::find(Auth::user()->id);


        return view('editProfile',compact('user'));

    }


}
