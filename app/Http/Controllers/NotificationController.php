<?php

namespace App\Http\Controllers;

use App\Notification;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;

class NotificationController extends Controller
{
    public function showNotifications()
    {
        $obj = new Notification();


        $obj->UpdateReadNotification();
        return view('notifications');
    }
}
