<?php

namespace App\Http\Controllers;

//use App;
use App\Photo;
use Auth;
use App\User;
use App\Classes\AddPhotoToUserProfile;
use Illuminate\Http\Request;
use App\Http\Requests\AddPhotoToRequest;
use App\Http\Requests;
use Intervention\Image\Facades\Image;
use File;

class PhotoController extends Controller
{
    public  function store( Request $request)
    {
//        $flyer = $this->user->publish(new App\Flyer($request->all()));

        $user = User::locatedAt(Auth::user()->name);

        $photo = $request->file('photo');

//        include(app_path().'\Classes\AddPhotoToUserProfile.php');

        (new AddPhotoToUserProfile( $user, $photo))->save();
    }

    public function postUploadImage(Request $request, User $userId )     // new function
    {
        $imageName = $request->file('image')->getClientOriginalName();

        $paths ='images/catalog' .'/'. $imageName;

        $thumbnailPath = 'images/catalog' .'/tn-'. $imageName;

//        $imageName = $request->file('image')->getClientOriginalExtension();

        $request->file('image')->move(base_path() . '/public/images/catalog/',$paths);


        Image::make($paths)
            ->fit(20)
            ->save($thumbnailPath);

//        \File::Delete($paths,!$request->image);
//        \File::Delete($paths,Input::except('password'));
       $pathAndThumbnailPath= Photo::where('user_id',Auth::user()->id)
            ->get();

//        dd($pathAndThumbnailPath);


        //create new entry in user_photo table
        if($pathAndThumbnailPath->isEmpty() ) {
            $pics = new Photo;

            $pics->name = $imageName;
            $pics->path = $paths;
            $pics->thumbnail_path = $thumbnailPath;
            $pics->user_id = Auth::user()->id;
            $pics->save();

        }else {
              //delete files from public directory
            foreach($pathAndThumbnailPath as $data) {
                $thumbnail = $data->thumbnail_path;
                $path = $data->path;
            }
            \File::Delete($thumbnail);
            \File::Delete($path);


            //update user_photo table method 1
//            Photo::where('user_id',Auth::user()->id)
//               ->update(['name' => $imageName,'path' => $paths,'thumbnail_path' => $thumbnailPath]);

            //update user_photo table method 2
            $userId->photos()->update(['name' => $imageName,'path' => $paths,'thumbnail_path' => $thumbnailPath]);

            }

//        storage::put('image',$path);
                return redirect('welcome');
//        return view('/welcome',compact('path'));
    }

}
