<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layout');
});


Route::auth();


Route::get('register/verify/{verifytoken}','RegisterController@confirm');

Route::get('welcome', 'HomeController@index');

Route::post('store/photo','PhotoController@store');

Route::post('uploadImage/{userId}','PhotoController@postUploadImage');

Route::get('contact', 'ContactController@showForm');

Route::post('contact', 'ContactController@sendContactInfo');

//Route::get('tweet','TwitterController@showTweet');

Route::post('tweet','TwitterController@CreateTweet');

Route::get('tweetPage','TwitterController@gettweetPage');

Route::get('follower','FollowingAndFollowerController@getFollowerPage');

Route::get('following','FollowingAndFollowerController@getFollowingPage');

Route::get('username/{name}','TwitterController@show');

//Route::post('username/{name}','NotificationController@notification');

Route::post('username/{name}','FollowingAndFollowerController@setUserFollowerRequest');

Route::get('edit/profile','RegisterController@viewEdit')->name('editUser');

Route::post('send/otp','RegisterController@otpVerification'); //new route

Route::get('create/otp','RegisterController@createOtp');

Route::get('notifications','NotificationController@showNotifications');

Route::any('find/users/{query}','SearchController@findSearch');

Route::post('find/users','TwitterController@fetchName');

Route::post('save/edit/page/{user}','RegisterController@updateRegister');

Route::get('sms','RegisterController@showOptPage');

Route::post('hide/button','TwitterController@closeButton');

Route::post('unhide/button','TwitterController@unHide');

Route::get('delete/comment{commentId}','TwitterController@deleteComments');

Route::post('user/comment','TwitterController@CreateComments');

Route::get('show/session',function(){dd(\Session::all());});

//Route::post('{FollowerUserID}','FollowingAndFollowerController@setUserFollowerRequest');

