<?php

namespace App\Http\Requests;
use App\User;
use App\Http\Requests\Request;
use Auth;

class AddPhotoToRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

//        dd(Auth::user());
        return User::where([
            'name' => Auth::user()->name,

//            'id' => $this->user()->id,
        ])->exists();
//        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimes:JPG,jpg,jpeg,bmp, png'

        ];
    }
}
