<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FollowerAndFollowing extends Model
{

    protected $fillable = ([
        'Following_user_id','Follower_user_id'
    ]);


    public static function getRecords($fieldName , $userId)
        {
            return FollowerAndFollowing::where($fieldName ,$userId)
                                        ->get();

        }



}
