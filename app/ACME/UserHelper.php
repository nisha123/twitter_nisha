<?php
namespace App\ACME;

use App\Notification;
use App\UserPostComment;
use Auth;
use App\User;
class UserHelper
{
    public function fetchFollowingNotificationId()
    {
        return Notification::where('Follower_notification_id', Auth::user()->id)->get()->sortByDesc('id');
    }

    public function getFollowerUserNameViaUserId($userId)
    {
        return User::where('id', $userId)
            ->value('name');

    }

    public static function countNotifications()
    {
//        return Notification::where('Follower_notification_id', Auth::user()->id)->get()->pluck('read')->count();
        return Notification::where('Follower_notification_id',Auth::user()->id)->where('read','=','0')->count();

    }

    public function countNewNotification()
    {
        return Notification::where('read','=','0')->count();

    }
    public function fetchReadTokenFromNotifications()
    {
        return Notification::where('Follower_notification_id', Auth::user()->id)->orderBy('id','desc')->value('read');
    }

    public function getCommentFromTweet($commentId)
    {
       return $varComment = UserPostComment::where('tweet_id',$commentId)->get();
    }
}
