<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class twitter extends Model
{
    protected $fillable = ([
        'tweet', 'user_id', 'hide'
    ]);

    public function tweets()
    {
        return $this->hasMany('App\twitter');
    }

    public static function fetchAllTweetsFromUserId($userId)
    {

        $NoOfUserFollowers = FollowerAndFollowing::where('Following_user_id', Auth::user()->id)
            ->get();
//        dd($NoOfUserFollowers);
        foreach ($NoOfUserFollowers as $followerId) {
            $variable[] = $followerId->Follower_user_id;
//                 dd($variable[]);


        }
        if (empty($variable)) {
            $allUserIds = [];
        } else {
            $allUserIds = array_merge($variable, [Auth::user()->id]);
        }
//dd($allUserIds);
        if ($allUserIds == null) {
            return twitter::where('user_id', Auth::user()->id)
                ->get()
                ->sortByDesc('id');
        } else {
            return twitter::whereIn('user_id', $allUserIds)
                ->get()
                ->sortByDesc('id');
        }
    }

    public static function fetchLoggedInUserTweets()
    {
        return twitter::where('user_id', Auth::user()->id)
            ->get()
            ->sortByDesc('id');
    }

    public static function calculateNoOfTweets($userId)
    {
        return twitter::where('user_id', $userId)
            ->get()
            ->count();
    }


    public static function getIdToDeleteData()
    {
//             $var =  array($userId);
//            dd($var);
//          $delete = twitter::find([$userId]);
        $delete = twitter::where('user_id', Auth::user()->id);

        $delete->delete();

    }

    public static function fetchAllIdFromTweet()
    {
        return twitter::where('user_id', Auth::user()->id)->value('id');


//            $id = twitter::where('user_id',Auth::user()->id)->get();
//            foreach($id as $idvalue)
//            {
//                $var[] = $idvalue->id;
//            }
//
//            return $var;
    }


}
