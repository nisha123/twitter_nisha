<?php

namespace App;

use Auth;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Input;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $primaryKey = 'id';

//    protected $fillable = 'User';

    protected $fillable = [

        'name',
        'email',
        'password',
        'verify_token',
        'address',
        'mobile_no',
        'status',
        'dob',
        'gender',
        'verify',
        'mobile_verify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function locatedAt($name)
    {

        return static::where(compact('name'))->first();
    }

    public function addPhoto(Photo $photo)
    {
        return $this->photos()->save($photo);
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }


    public static function fetchNameFromUserTable()
    {
        return User::get(['name']);
    }

    public static function fetchMobileVerifyTokenFromTable($userId)
    {
        return User::where('id', $userId)
            ->value('mobile_verify');
    }

    //object injection method
//    public function getFollowerUserNameViaUserId($userId)
//    {
//        return User::where('id', $userId)
//            ->value('name');
//
//    }

    public static function sendQuery($query)
    {
        return $query;
    }


}