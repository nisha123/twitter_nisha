<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

class HideAndShow extends Model
{
    protected $fillable = [
        'tweet_tag', 'user_tag'

    ];

    public static function fetchTweetTag($findId ,$userId)
    {
        $var = twitter::where('user_id',$findId)->where('id', $userId)->value('hide');
        $str2 = substr($var, 1);
        $str3 = explode(",",$str2) ;
        return $str3;
    }

}
